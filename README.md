# 使用说明

## 目录说明

.
├── README.md 说明文档
├── content.json 模块入参的Props的JsonSchema
├── data.json 模块入参默认数据
├── index.js 总入口文件
├── manifest.json 模块所需清单（其他模块、script引入的js、图片等）
├── package.json 项目描述
└── src
    ├── index.html 页面html
    ├── index.less 页面less
    ├── index.tsx 模块入口文件